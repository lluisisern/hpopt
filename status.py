#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import grp
import os
import pwd

import boto3
import daemon
import daemon.pidlockfile

import pipeline


def main():
    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["status"]["sqs"])
    while True:
        for message in sqs.receive_messages():
            message.delete()
            print(message.body)


if __name__ == "__main__":
    with open("/var/log/hpopt/status.log", "w") as fd:
        with daemon.DaemonContext(uid=pwd.getpwnam("hpopt").pw_uid,
                                  gid=grp.getgrnam("hpopt").gr_gid,
                                  pidfile=daemon.pidlockfile.PIDLockFile(os.path.join(os.environ["HOME"], ".status.pid")),
                                  stdout=fd, stderr=fd):
            main()

