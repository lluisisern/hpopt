#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import os
import pwd
import shlex
import subprocess
import sys

import pipeline


def sync_dir(bucket,
             exclude_subdirectories=False, delete=False,
             sources=(),
             remote_src=None, remote_dst=None, local_src=None, local_dst=None):
    src, dst = None, None
    if remote_src is not None and local_src is not None: raise ValueError("ambiguous source")
    if remote_dst is not None and local_dst is not None: raise ValueError("ambiguous destination")
    if remote_src is not None and local_dst is not None: src, dst = make_s3_url(bucket, remote_src), local_dst
    if local_src is not None and remote_dst is not None: src, dst = local_src, make_s3_url(bucket, remote_dst)
    if src is None or dst is None: raise ValueError("a valid source and destination must be specified")
    command = ["aws", "s3", "sync", shlex.quote(src), shlex.quote(dst)]
    if delete: command += ["--delete"]
    if exclude_subdirectories: command += ["--exclude", "*/*"]
    if len(sources):
        command += ["--exclude", "*"]
        for source in sources: command += ["--include", source]
    subprocess.call(command)


def is_s3_url(url):
    return url.startswith("s3://")


def parse_s3_url(url):
    if not is_s3_url(url):
        raise ValueError("expected URL beginning with s3://")
    url = os.path.split(url[len("s3://"):])
    return {"bucket": url[0], "path": os.path.join(url[1:])}


def make_s3_url(bucket, path):
    return "s3://{}/{}".format(bucket, path)


def main():
    sync_dir(pipeline.names["config"]["s3"], delete=True,
             remote_src="config",
             local_dst=os.path.join(os.environ["HOME"], "config"))


if __name__ == "__main__":
    if pwd.getpwnam("hpopt").pw_uid != os.getuid():
        print("please run this script as user hpopt (uid={})".format(pwd.getpwnam("hpopt").pw_uid))
        sys.exit(1)
    try: main()
    except KeyboardInterrupt: pass
