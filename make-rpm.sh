#!/bin/bash

version=$(cat rpm/version)
release=$(cat rpm/release)

tmpdir=$(mktemp -d -t "hpopt-XXXXXX")
tarname=hpopt-${version}-${release}.tar.gz
tardir=${tmpdir}/hpopt-${version}

mkdir ${tardir}
rsync -arv \
    --include="*.py" \
    --include="initscripts" \
    --include="initscripts/*" \
    --include="requirements.txt" \
    --include="pipeline-names.json" \
    --exclude="*" \
    ./ ${tardir}
tar -C ${tmpdir} -czvf ~/rpmbuild/SOURCES/${tarname} $(basename ${tardir})
rm -rv ${tmpdir}

cp -v rpm/hpopt-rpmbuild.spec ~/rpmbuild/SPECS/hpopt-${version}.spec

sed -i -e "s/%VERSION%/${version}/g" ~/rpmbuild/SPECS/hpopt-${version}.spec
sed -i -e "s/%RELEASE%/${release}/g" ~/rpmbuild/SPECS/hpopt-${version}.spec

rpmbuild -v -ba ~/rpmbuild/SPECS/hpopt-${version}.spec
cp -v ~/rpmbuild/RPMS/noarch/hpopt-${version}-${release}.noarch.rpm \
    rpm/hpopt-latest.rpm
