#!/usr/bin/env python3
#-*-coding: utf-8-*-

import random
import sys
import time

sys.path.append("/usr/local/pandora/bin")
sys.path.append("/usr/local/pandora/lib")
import pyPandora as pandora


class RandomAgent(pandora.Agent):

    def __init__(self, agent_id):
        super().__init__(agent_id)

    def selectActions(self):
        # move
        newPosition = self.position
        newPosition._x += random.randint(-1, 1)
        newPosition._y += random.randint(-1, 1)
        if self.getWorld().checkPosition(newPosition):
            self.position = newPosition

    def serialize(self):
        pass


class RandomWorld(pandora.World):

    def __init__(self, config):
        super().__init__(config)
        self.numAgents = config.numAgents

    def createAgents(self):
        for i in range(self.numAgents):
            agent = RandomAgent("RandomAgent_{}".format(i))
            self.addAgent(agent)
            agent.setRandomPosition()


class RandomWorldConfig(pandora.Config):

    def __init__(self, xmlFile):
        super().__init__(xmlFile)
        self.numAgents = 0

    def loadParams(self):
        self.numAgents = self.getParamInt("numAgents", "value")


if __name__=="__main__":

    if len(sys.argv)>2:
        print("USAGE: randomWalkers [config file]")
        sys.exit(1)
    fileName = "config.xml"
    if len(sys.argv)!=1:
        fileName = sys.argv[1]

    print("running simulation...")
    start_time = time.time()

    with open(fileName) as fd:
        print(fd.read())

    world = RandomWorld(RandomWorldConfig(fileName))
    world.initialize()
    world.run()

    total_time = time.time() - start_time
    print("done! simulation ran for {} seconds".format(total_time))
