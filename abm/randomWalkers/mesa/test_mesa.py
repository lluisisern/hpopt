#!/usr/bin/env python3
#-*-coding: utf-8-*-

import random
import sys
import time

import mesa
import mesa.batchrunner
import mesa.datacollection
import mesa.space
import mesa.time
import untangle


def config(xmlfile):
    xml = untangle.parse(xmlfile)
    return {"width" : int(xml.config.size["width"]),
            "height": int(xml.config.size["height"]),
            "steps" : int(xml.config.numSteps ["value"]),
            "agents": int(xml.config.numAgents["value"])}


class RandomWorld(mesa.Model):

    def __init__(self, num_agents, width=100, height=100, seed=None):
        super().__init__(seed)
        self.num_agents = num_agents
        self.grid = mesa.space.MultiGrid(width, height, False)
        self.schedule = mesa.time.RandomActivation(self)
        self.datacollection = mesa.datacollection.DataCollector(
            agent_reporters={"Agent position": lambda agent: agent.pos})
        self.running = True

        for i in range(self.num_agents):
            agent = RandomAgent("RandomAgent_{}".format(i), self)
            self.schedule.add(agent)
            self.grid.place_agent(agent, (random.randrange(width),
                                          random.randrange(height)))

    def step(self):
        self.datacollection.collect(self)
        self.schedule.step()


class RandomAgent(mesa.Agent):

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.pos = ()

    def step(self):
        self.model.grid.move_agent(self,
                                   random.choice(self.model.grid.get_neighborhood(self.pos,
                                                                                  moore=True,
                                                                                  include_center=False)))


if __name__=="__main__":

    print("running simulation...")
    start_time = time.time()

    if len(sys.argv)>2:
        print("USAGE: randomWalkers [config file]")
        sys.exit(1)
    xmlfile = "config.xml"
    if len(sys.argv)!=1:
        xmlfile = sys.argv[1]

    with open(xmlfile) as fd:
        print(fd.read())

    conf = config(xmlfile)

    batch_run = mesa.batchrunner.BatchRunner(RandomWorld,
                                             dict(num_agents=conf["agents"],
                                                  width =conf["width"],
                                                  height=conf["height"]),
                                             iterations=1,
                                             max_steps=conf["steps"])
    batch_run.run_all()

    total_time = time.time()-start_time
    print("done! simulation ran for {} seconds".format(total_time))
