
#include <RandomAgent.hxx>
#include <MoveAction.hxx>
#include <Statistics.hxx>
#include <World.hxx>

namespace Examples
{

RandomAgent::RandomAgent( const std::string & id ) : Agent(id)
{
}

RandomAgent::~RandomAgent()
{
}

void RandomAgent::selectActions()
{
    _actions.push_back(new MoveAction());
}

} // namespace Examples

