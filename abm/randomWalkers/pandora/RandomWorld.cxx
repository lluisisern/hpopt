
#include <RandomWorld.hxx>

#include <RandomWorldConfig.hxx>
#include <RandomAgent.hxx>
#include <DynamicRaster.hxx>
#include <Point2D.hxx>
#include <GeneralState.hxx>
#include <Logger.hxx>

namespace Examples 
{

RandomWorld::RandomWorld(Engine::Config * config, Engine::Scheduler * scheduler ) : World(config, scheduler, false)
{
}

RandomWorld::~RandomWorld()
{
}

void RandomWorld::createRasters()
{
}

void RandomWorld::createAgents()
{
    const RandomWorldConfig& randomConfig = (const RandomWorldConfig&)getConfig();
    for(int i=0; i<randomConfig._numAgents; i++)
    {
        std::ostringstream oss;
        oss << "RandomAgent_" << i;
        RandomAgent * agent = new RandomAgent(oss.str());
        addAgent(agent);
        agent->setRandomPosition();
    }
}

} // namespace Examples

