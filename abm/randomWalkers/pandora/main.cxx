
#include <RandomWorld.hxx>
#include <RandomWorldConfig.hxx>
#include <Exception.hxx>
#include <Config.hxx>

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <chrono>

int main(int argc, char *argv[])
{
    try
    {
        if(argc>2)
        {
            throw Engine::Exception("USAGE: randomWalkers [config file]");
        }
        std::string fileName("config.xml");
        if(argc!=1)
        {
            fileName = argv[1];
        }

        std::cout<<"running simulation..."<<std::endl;
        auto startTime = std::chrono::steady_clock::now();

        std::ifstream conf(fileName);
        std::cout<<conf.rdbuf()<<std::endl;

        Examples::RandomWorld world(new Examples::RandomWorldConfig(fileName));

        world.initialize(argc, argv);
        world.run();

        auto elapsed = std::chrono::steady_clock::now() - startTime;
        double totalTime = double(elapsed.count())*std::chrono::steady_clock::period::num/std::chrono::steady_clock::period::den;
        std::cout<<"done! simulation ran for "<<totalTime<<" seconds"<<std::endl;
    }
    catch( std::exception & exceptionThrown )
    {
        std::cout << "exception thrown: " << exceptionThrown.what() << std::endl;
    }
    return 0;
}

