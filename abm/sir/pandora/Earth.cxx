#include <Earth.hxx>

#include <Hospital.hxx>
#include <EarthConfig.hxx>
#include <Human.hxx>
#include <DynamicRaster.hxx>
#include <Point2D.hxx>
#include <GeneralState.hxx>
#include <Logger.hxx>
#include <typedefs.hxx>

#include <cmath>
#include <fstream>

namespace Examples
{

Earth::Earth( EarthConfig * config, Engine::Scheduler * scheduler ) : World(config, scheduler, true)
{
}

Earth::~Earth()
{
}

void Earth::createRasters()
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    registerStaticRaster("dem", true, eDem);
    Engine::GeneralState::rasterLoader().fillGDALRaster(getStaticRaster("dem"), earthConfig._demName, getBoundaries());

    registerStaticRaster("population", true, ePopulation);
    Engine::GeneralState::rasterLoader().fillGDALRaster(getStaticRaster("population"), earthConfig._populationName, getBoundaries());

    registerDynamicRaster("susceptibles", true, eSusceptibles);
    getDynamicRaster("susceptibles").setInitValues(0, std::numeric_limits<int>::max(), 0);

    registerDynamicRaster("infected", true, eInfected);
    getDynamicRaster("infected").setInitValues(0, std::numeric_limits<int>::max(), 0);

    registerDynamicRaster("recovered", true, eRecovered);
    getDynamicRaster("recovered").setInitValues(0, std::numeric_limits<int>::max(), 0);

    registerDynamicRaster("hospital", true, eHospital);
    getDynamicRaster("hospital").setInitValues(0, std::numeric_limits<int>::max(), 0);
}

void Earth::createAgents()
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    std::stringstream logName;
    logName << "simulation_" << getId();
    log_INFO(logName.str(), getWallTime() << " creating agents");

    Engine::Point2D<int> index;
    int totalHumans = 0;
    int indexHumans = 0;
    for (auto index : getBoundaries())
    {
        int numSusceptibles = getValue(ePopulation, index);
        if (numSusceptibles>0)
        {
            int scaledSusceptibles = 0;
            for (int i=0; i<numSusceptibles; i=i+earthConfig._scale)
            {
                std::ostringstream oss;
                oss << "Human_" << indexHumans;
                Human* newHuman = new Human(oss.str());
                addAgent(newHuman);
                newHuman->setPosition(index);
                scaledSusceptibles++;
                indexHumans++;
            }
            totalHumans += scaledSusceptibles;
            log_DEBUG(logName.str(), getWallTime() << " created: " << scaledSusceptibles << " susceptibles at pos: " << index);
            setValue(eSusceptibles, index, scaledSusceptibles);
        }
    }
    log_INFO(logName.str(), getWallTime() << " " << indexHumans << " susceptibles created");

    for (int i=0; i<earthConfig._numCases; i++)
    {
        std::ostringstream oss;
        oss << "Human_" << indexHumans;
        indexHumans++;
        Human* newInfected = new Human(oss.str(), true);
        addAgent(newInfected);
        newInfected->setPosition(earthConfig._firstCase);
    }

    if (getBoundaries().contains(earthConfig._firstCase))
    {
        setValue(eInfected, earthConfig._firstCase, earthConfig._numCases);
        log_INFO(logName.str(), getWallTime() << " " << earthConfig._numCases << " cases created at infection focus: " << earthConfig._firstCase);
    }

    int x, y, maxServed;
    int indexHospitals = 0;
    std::ifstream infile(earthConfig._hospitalsName);
    while (infile>>x>>y>>maxServed)
    {
        std::ostringstream oss;
        oss << "Hospital_" << indexHospitals;
        Hospital* newHospital = new Hospital(oss.str(), indexHospitals);
        addAgent(newHospital);
        _hospitals.push_back(newHospital);
        Engine::Point2D<int> position(x,y);
        newHospital->setPosition(position);
        newHospital->setMaxServed(getMaxRadius());
        indexHospitals++;
    }
    infile.close();

    for (auto index : getBoundaries())
    {
        if (getValue(eDem, index)>-1)
        {
            double distance;
            int nearest = getNearestHospital(index, distance)->getIndex();

            if (distance<=getMaxRadius())
            {
                getDynamicRaster("hospital").setValue(index, nearest);
            }
            else
            {
                getDynamicRaster("hospital").setValue(index, -1);
            }
        }
        else
        {
            getDynamicRaster("hospital").setValue(index, -1);
        }
    }
}

float Earth::getVirulence() const
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    return earthConfig._virulence;
}

float Earth::getRecovery() const
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    return earthConfig._recovery;
}

float Earth::getRecoveryMultiplier() const
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    return earthConfig._recoveryMultiplier;
}

float Earth::getMaxRadius() const
{
    const EarthConfig & earthConfig = (const EarthConfig&)getConfig();
    return earthConfig._maxRadius;
}

double getDistance(const Engine::Point2D<int>& pos1, const Engine::Point2D<int>& pos2)
{
    return sqrt(std::pow(pos1._x - pos2._x, 2.0) +
                std::pow(pos1._y - pos2._y, 2.0));
}

Hospital* Earth::getNearestHospital(const Engine::Point2D<int>& position, double& distance) const
{
    double minDist = std::numeric_limits<double>::max();
    Hospital* nearest = nullptr;
    for (Hospital* hospital: _hospitals)
    {
        double dist = getDistance(position, hospital->getPosition());
        if (dist<minDist)
        {
            minDist = dist;
            nearest = hospital;
        }
    }

    distance = minDist;
    return nearest;
}

Hospital* Earth::getHospitalByIndex(int index) const
{
    for (Hospital* hospital: _hospitals)
    {
        if (hospital->getIndex() == index)
        {
            return hospital;
        }
    }
    return nullptr;
}

void Earth::stepEnvironment()
{
    std::stringstream logName;
    logName << "simulation_" << getId();
    int totalSusceptibles = 0;
    int totalInfected = 0;
    int totalRecovered = 0;

    for (auto index : getBoundaries())
    {
        setValue(eSusceptibles, index, 0);
        setValue(eInfected, index, 0);
        setValue(eRecovered, index, 0);
    }
    for (Engine::AgentsList::const_iterator it=beginAgents(); it!=endAgents(); it++)
    {
        if (!(*it)->exists() || !getBoundaries().contains((*it)->getPosition()))
        {
            continue;
        }
        if ((*it)->isType("Susceptible"))
        {
            int previousSusceptibles = getValue(eSusceptibles, (*it)->getPosition());
            setValue(eSusceptibles, (*it)->getPosition(), previousSusceptibles+1);
            totalSusceptibles++;
        }
        else if ((*it)->isType("Infected"))
        {
            int previousInfected = getValue(eInfected, (*it)->getPosition());
            setValue(eInfected, (*it)->getPosition(), previousInfected+1);
            totalInfected++;
        }
        else if ((*it)->isType("Recovered"))
        {
            int previousRecovered = getValue(eRecovered, (*it)->getPosition());
            setValue(eRecovered, (*it)->getPosition(), previousRecovered+1);
            totalRecovered++;
        }
        else if ((*it)->isType("Hospital"))
        {
            std::static_pointer_cast<Hospital>(*it)->setServed(0);
        }
    }
    log_INFO(logName.str(), getWallTime()<<" step: "<<_step<<
        " susceptibles: "<<totalSusceptibles<<" infected: "<<totalInfected<<" recovered: "<<totalRecovered);
}

} // namespace Examples

