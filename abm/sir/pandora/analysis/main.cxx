/*
 * Copyright (c) 2012
 * COMPUTER APPLICATIONS IN SCIENCE & ENGINEERING
 * BARCELONA SUPERCOMPUTING CENTRE - CENTRO NACIONAL DE SUPERCOMPUTACIÓN
 * http://www.bsc.es

 * This file is part of Pandora Library. This library is free software; 
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * Pandora is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "Exception.hxx"
#include "SimulationRecord.hxx"

#include "analysis/GlobalRasterStats.hxx"
#include "analysis/RasterMean.hxx"
#include "analysis/RasterSum.hxx"

#include <iostream>
#include <fstream>

float renormalize(const float x,
    const float old_min, const float old_max,
    const float new_min, const float new_max)
{
    return (((x - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min;
}

void getCoords(const Engine::Point2D<int>& position, float& lon, float& lat)
{
    lon = renormalize(position._x, 0, 259, 0.1591812, 3.3323241);
    lat = renormalize(position._y, 0, 259, 42.8615226, 40.5230524);
}

int main(int argc, char *argv[])
{
    try
    {
        Engine::SimulationRecord simRecord(1, false);
        simRecord.loadHDF5("../data/epidemy.h5", true, true);

        PostProcess::GlobalRasterStats rasterStats;
        rasterStats.addAnalysis(new PostProcess::RasterSum());

        rasterStats.apply(simRecord, "susceptibles.csv", "susceptibles");
        rasterStats.apply(simRecord, "infected.csv", "infected");
        rasterStats.apply(simRecord, "recovered.csv", "recovered");

        const Engine::SimulationRecord::RasterHistory& susceptibles = simRecord.getRasterHistory("susceptibles");
        const Engine::SimulationRecord::RasterHistory& infected = simRecord.getRasterHistory("infected");
        const Engine::SimulationRecord::RasterHistory& recovered = simRecord.getRasterHistory("recovered");

        std::ofstream output("steps.csv");
        std::string sep(";");
        output<<"step"<<sep<<"lon"<<sep<<"lat"<<sep<<"x"<<sep<<"y"<<sep<<"susceptibles"<<sep<<"infected"<<sep<<"recovered"<<std::endl;

        const Engine::Size<int>& size = simRecord.getSize();
        for (int step = 0; step<simRecord.getNumSteps(); ++step)
        {
            for (int x = 0; x<size._width; ++x)
            {
                for (int y = 0; y<size._height; ++y)
                {
                    Engine::Point2D<int> position(x, y);
                    float lon, lat;
                    getCoords(position, lon, lat);

                    int s = susceptibles[step].getValue(position);
                    int i = infected[step].getValue(position);
                    int r = recovered[step].getValue(position);

                    if ((s<=0) && (i<=0) && (r<=0)) continue;

                    output<<step<<sep<<lon<<sep<<lat<<sep<<x<<sep<<y<<sep<<s<<sep<<i<<sep<<r<<sep<<std::endl;
                }
            }
        }

        output.close();

        std::stringstream curlCall;
        curlCall<<"curl -v -F file=@steps.csv \"https://lluisisern.carto.com/api/v1/imports/?api_key=";
        curlCall<<argv[1]<<"\"";
        system(curlCall.str().c_str());
        std::cout<<std::endl;
    }
    catch( std::exception & exceptionThrown )
    {
        std::cout << "exception thrown: " << exceptionThrown.what() << std::endl;
        return -1;
    }
    return 0;
}

