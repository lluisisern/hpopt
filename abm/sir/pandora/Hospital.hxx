#ifndef __Hospital_hxx__
#define __Hospital_hxx__

#include <Agent.hxx>
#include <Human.hxx>

#include <string>

namespace Examples
{

class Hospital : public Engine::Agent
{
    int _index;
    int _served; // MpiAttribute
    int _maxServed;
public:
    Hospital(const std::string& id, int index);
    virtual ~Hospital();

    void updateKnowledge();
    void selectActions();
    void updateState();
    void registerAttributes();
    void serialize();

    int getIndex() const;
    int getServed() const;
    void setServed(int);
    int getMaxServed() const;
    void setMaxServed(int);
    bool requestAntidote();


    ////////////////////////////////////////////////
    // This code has been automatically generated //
    /////// Please do not modify it ////////////////
    ////////////////////////////////////////////////
    Hospital( void * );
    void * fillPackage();
    void sendVectorAttributes(int);
    void receiveVectorAttributes(int);
    ////////////////////////////////////////////////
    //////// End of generated code /////////////////
    ////////////////////////////////////////////////

};

} // namespace Examples

#endif // __Hospital_hxx__

