#include <Hospital.hxx>
#include <DoNothingAction.hxx>
#include <GeneralState.hxx>

//#include <algorithm>

namespace Examples
{

Hospital::Hospital(const std::string& id, int index):
    Agent(id),
    _served(0),
    _maxServed(0),
    _index(index)
{
    changeType("Hospital");
}

Hospital::~Hospital()
{
}

void Hospital::updateKnowledge()
{
}

void Hospital::selectActions()
{
    _actions.push_back(new DoNothingAction());
}

void Hospital::updateState()
{

}

void Hospital::registerAttributes()
{
    registerIntAttribute("served");
}

void Hospital::serialize()
{
    serializeAttribute("served", _served);
}

bool Hospital::requestAntidote()
{
    if (_served>=_maxServed)
        return false;
    ++_served;
    return true;
}

int Hospital::getServed() const
{
    return _served;
}

void Hospital::setServed(int served)
{
    _served = served;
}

int Hospital::getMaxServed() const
{
    return _maxServed;
}

void Hospital::setMaxServed(int maxServed)
{
    _maxServed = maxServed;
}

int Hospital::getIndex() const
{
    return _index;
}

} // namespace Examples

