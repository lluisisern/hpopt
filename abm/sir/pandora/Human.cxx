#include <Human.hxx>
#include <MoveAction.hxx>
#include <Statistics.hxx>
#include <World.hxx>
#include <GeneralState.hxx>
#include <Logger.hxx>
#include <Statistics.hxx>
#include <Earth.hxx>

#include <DoNothingAction.hxx>
#include <MoveAction.hxx>

namespace Examples
{

Human::Human(const std::string& id, bool infected):
    Agent(id),
    _threatLevel(0.0f),
    _infected(infected),
    _recovered(false),
    _hasSusceptibles(false)
{
    if (!_infected)
        changeType("Susceptible");
    else
        changeType("Infected");
}

Human::~Human()
{
}

void Human::registerAttributes()
{
    registerIntAttribute("threatLevel");
}

void Human::updateKnowledge()
{
    if (_recovered)
    {
        _threatLevel = 0.0f;
    }
    else
    {
        int numSusceptibles = getWorld()->getValue(eSusceptibles, _position);
        int numInfected = getWorld()->getValue(eInfected, _position);

        if (_infected)
        {
            _hasSusceptibles = (numSusceptibles>0);
        }
        else // susceptible
        {
            if (numInfected==0)
                _threatLevel = 0.0f;
            else
            {
                --numSusceptibles;
                _threatLevel = (float)numInfected/(float)numSusceptibles;

                std::stringstream logName;
                logName << "agents_" << _world->getId();
                log_DEBUG(logName.str(), this<<" with: "<<numSusceptibles<<" humans and: "<<numInfected<<" infected with threat level: "<<_threatLevel);
            }
        }
    }
}

void Human::selectActions()
{
    if (_recovered || (!_infected))
    {
        _actions.push_back(new DoNothingAction());
    }
    else if (_infected)
    {
        if (_hasSusceptibles)
        {
            if (Engine::GeneralState::statistics().getUniformDistValue() > _threatLevel)
                _actions.push_back(new MoveAction());
            else
                _actions.push_back(new DoNothingAction());
        }
        else
        {
            _actions.push_back(new MoveAction());
        }
    }
}

void Human::updateState()
{
    if (_recovered)
    {
        return;
    }

    Earth& world = (Earth&)getWorldRef();

    if (_infected)
    {
        float probabilityRecovery = world.getRecovery();
        float recoveryMultiplier = world.getRecoveryMultiplier();

        int nearestIndex = world.getValue("hospital", _position);
        if (nearestIndex>-1)
        {
            Hospital* nearestHospital = world.getHospitalByIndex(nearestIndex);
            if (nearestHospital->requestAntidote())
                probabilityRecovery *= recoveryMultiplier;
        }

        if (Engine::GeneralState::statistics().getUniformDistValue() < probabilityRecovery)
        {
            _infected = false;
            _recovered = true;
            changeType("Recovered");

            std::stringstream logName;
            logName << "agents_" << world.getId();
            log_DEBUG(logName.str(), this<<" recovered!");
        }
    }
    else // susceptible
    {
        float probabilityAttack = _threatLevel * world.getVirulence();
        if (Engine::GeneralState::statistics().getUniformDistValue() < probabilityAttack)
        {
            _infected = true;
            changeType("Infected");
            log_DEBUG(logName.str(), this<<" had threat level: "<<_threatLevel<<" got infected!");
        }
    }
}

void Human::serialize()
{
    if (_infected)
        serializeAttribute("threatLevel", 0);
    else
        serializeAttribute("threatLevel", (int)(_threatLevel*100.0f));
}

} // namespace Examples

