#ifndef __Earth_hxx__
#define __Earth_hxx__

#include <World.hxx>
#include <Hospital.hxx>

namespace Examples
{

enum Rasters
{
    eDem,
    ePopulation,
    eSusceptibles,
    eInfected,
    eRecovered,
    eHospital
};

class EarthConfig;

class Earth : public Engine::World
{
    void createRasters();
    void createAgents();

    std::vector<Hospital*> _hospitals;
    void stepEnvironment();
public:
    Earth( EarthConfig* config, Engine::Scheduler* scheduler = 0);
    virtual ~Earth();

    float getVirulence() const;
    float getRecovery() const;
    float getRecoveryMultiplier() const;
    float getMaxRadius() const;
    Hospital* getNearestHospital(const Engine::Point2D<int>& position, double& distance) const;
    Hospital* getHospitalByIndex(int index) const;
};

} // namespace Examples 

#endif // __Earth_hxx__

