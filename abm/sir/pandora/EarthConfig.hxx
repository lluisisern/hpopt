#ifndef __EarthConfig_hxx__
#define __EarthConfig_hxx__

#include <Config.hxx>
#include <Point2D.hxx>
#include <Size.hxx>
#include <vector>

namespace Examples
{

class EarthConfig : public Engine::Config
{
    std::vector<Engine::Point2D<int>> hospitals;

    std::string _demName;
    std::string _populationName;
    std::string _hospitalsName;

    // number of real individuals to 1 in simulation
    int _scale;
    Engine::Point2D<int> _firstCase;
    int _numCases;
    float _virulence;
    float _recovery;
    float _recoveryMultiplier;
    float _maxRadius;

public:
    EarthConfig( const std::string & xmlFile );
    virtual ~EarthConfig();

    void loadParams();
    friend class Earth;
};

} // namespace Examples

#endif // __EarthConfig_hxx__

