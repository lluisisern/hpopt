#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys

import boto3

if __name__ == "__main__":

    with open("pipeline-names.json") as fd:
        names = json.load(fd)
    message = json.loads(sys.argv[1])

    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=names["status"]["sqs"])
    sqs.send_message(MessageBody=json.dumps({"name": message["scrape"]["name"],
                                             "type": "scrape",
                                             "status": "success"}))
