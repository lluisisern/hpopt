#!/bin/bash
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

apt-get -y update
apt-get -y upgrade
apt-get -y install packaging-dev python3 python3-dev python3-pip python3-matplotlib \
    python3-pandas awscli libtinyxml-dev libdevil-dev freeglut3-dev libqwt-dev libqwt6 \
    libqt4-dev libqt4-opengl-dev libgdal1-dev mpich2 scons build-essential \
    libboost-random-dev libboost-python-dev libboost-filesystem-dev libboost-system-dev \
    libmpich2-dev libboost-test-dev libboost-timer-dev libboost-chrono-dev git
pip3 install mesa untangle

tests_bucket="s3://hpopt-abm-tests"

mkdir ~/hdf5
cd ~/hdf5
wget "https://support.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.10.0-patch1.tar.bz2"
tar xf hdf5-1.10.0-patch1.tar.bz2
cd hdf5-1.10.0-patch1
./configure --enable-parallel --prefix="/usr/local/hdf5" --disable-shared --with-pic
make
make install

mkdir ~/pandora-build
cd ~/pandora-build
git clone "https://github.com/xrubio/pandora.git" .
scons
scons install

export PANDORAPATH=/usr/local/pandora
export PATH=$PATH:$PANDORAPATH/bin/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PANDORAPATH/lib/

cd ~
for i in pandora pypandora mesa; do
    aws s3 --region eu-west-1 cp ${tests_bucket}/tests/${i}.tar.xz .
    tar xf ${i}.tar.xz
done

cd ~/pandora
scons
for i in $(seq 0 3); do
    ./test_pandora config${i}.xml > results${i}.txt
done

cd ~/pypandora
for i in $(seq 0 3); do
    ./test_pandora.py config${i}.xml > results${i}.txt
done

cd ~/mesa
for i in $(seq 0 3); do
    ./test_mesa.py config${i}.xml > results${i}.txt
done

cd ~
inst_type=$(curl http://169.254.169.254/latest/meta-data/instance-type)
now=$(date +"%d_%m_%Y_%H%M%S")
for i in $(seq 0 3); do
    for j in pandora pypandora mesa; do
        aws s3 --region eu-west-1 cp ~/${j}/results${i}.txt ${tests_bucket}/${inst_type}-${now}/${j}/
    done
done
aws s3 --region eu-west-1 cp /var/log/cloud-init-output.log ${tests_bucket}/${inst_type}-${now}/

poweroff
