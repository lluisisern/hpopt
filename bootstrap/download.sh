#!/bin/bash
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

# install code and deps
tmpdir=$(mktemp -d)
pushd $tmpdir
aws s3 cp s3://isern-tfg-deploy/hpopt-latest.rpm .
yum -y localinstall hpopt-latest.rpm
popd
rm -r $tmpdir
pip-3.4 install -r /usr/share/hpopt/requirements.txt
# configure aws
sudo -u hpopt aws configure set region eu-west-1
sudo -u hpopt hpopt-sync
# start daemon
chkconfig --add hpopt-download
service hpopt-download start
