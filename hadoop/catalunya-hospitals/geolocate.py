#!/usr/bin/env python3
# -*-coding: utf-8-*-

import json
import sys
import time
import requests

def renormalize(x, old_min, old_max, new_min, new_max):
    return (((x - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min

# g_{min,max}_{x,y} is the bounding box of Catalonia, and
# l_{min,max}_{x,y} are the limits of the epidemy Pandora model
def translate_to_model(x, y,
                       g_min_x=42.8615226, g_max_x=40.5230524, g_min_y=0.1591812, g_max_y=3.3323241,
                       l_min_x=0, l_max_x=259, l_min_y=0, l_max_y=259):
    return {"model_x": renormalize(x, g_min_y, g_max_y, l_min_y, l_max_y),
            "model_y": renormalize(y, g_min_x, g_max_x, l_min_x, l_max_x)}

def nominatim(query, format="json"):
    if nominatim.last_query == 0: # first query, initialize counter
        nominatim.last_query = time.time()
    if (time.time() - nominatim.last_query) <= 1: # limit rate to Nominatim to 1 query/sec
        print("throttling Nominatim query, please wait... ('{}')".format(query))
        time.sleep(1)
    r = requests.get("http://nominatim.openstreetmap.org/search",
                                            params={"q": query,
                                                    "format": format,
                                                    "state": "Catalonia"},
                                            headers={"User-Agent": "hpopt simple Nominatim scraper, v1.0"})
    response = json.loads(r.text)
    nominatim.last_query = time.time()
    if len(response) > 1:
        print("WARNING: '{}' returned more than one result".format(query))
    if len(response) > 0:
        return {"lon": response[0]["lon"], "lat": response[0]["lat"]}
nominatim.last_query = 0

def main():
    if len(sys.argv) != 3:
        print("usage: {} hospitals.json_in hospitals.json_out".format(sys.argv[0]))
        sys.exit(1)
    with open(sys.argv[1]) as fd1:
        hospitals = json.load(fd1)
    try:
        with open(sys.argv[2]) as fd2:
            hospitals_existing = json.load(fd2)
    except FileNotFoundError:
        hospitals_existing = {}
    for name in hospitals:
        hospital = hospitals[name]
        hospital_existing = hospitals_existing[name] if name in hospitals_existing else None
        if hospital_existing is None or "lon" not in hospital_existing or "lat" not in hospital_existing:
            geoloc = nominatim(name)
            if geoloc:
                model_coords = translate_to_model(float(geoloc["lon"]), float(geoloc["lat"]))
            else:
                print("WARNING: '{}' did not return any results".format(name))
                # make sure we don't make this query again!
                geoloc = {"lon": None, "lat": None}
                model_coords = {"model_x": None, "model_y": None}
            hospital["lon"] = geoloc["lon"]
            hospital["lat"] = geoloc["lat"]
            hospital["model_x"] = model_coords["model_x"]
            hospital["model_y"] = model_coords["model_y"]
        else:
            hospital["lon"] = hospital_existing["lon"]
            hospital["lat"] = hospital_existing["lat"]
            if "model_x" in hospital_existing and "model_y" in hospital_existing:
                hospital["model_x"] = hospital_existing["model_x"]
                hospital["model_y"] = hospital_existing["model_y"]
            elif hospital_existing["lon"] is not None and hospital_existing["lat"] is not None:
                model_coords = translate_to_model(float(hospital["lon"]), float(hospital["lat"]))
                hospital["model_x"] = model_coords["model_x"]
                hospital["model_y"] = model_coords["model_y"]
            else:
                hospital["model_x"] = None
                hospital["model_y"] = None
    with open(sys.argv[2], "w") as fd:
        json.dump(hospitals, fd, ensure_ascii=False, indent=2)

if __name__ == "__main__":
    main()
