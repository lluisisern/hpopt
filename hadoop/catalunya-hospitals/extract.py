#!/usr/bin/env python3
# -*-coding: utf-8-*-

import json
import sys

def main():
    if len(sys.argv)!=4:
        print("usage: {} metrics.json hospitals-geoloc.json hospitals.csv".format(sys.argv[0]))
        sys.exit(1)
    with open(sys.argv[1]) as fd:
        metrics = json.load(fd)
    with open(sys.argv[2]) as fd:
        hospitals = json.load(fd)
    rows = []
    for name in hospitals:
        hospital = hospitals[name]
        try:
            metric = metrics[name]["Temps a la llista d'espera (%)"]
        except KeyError:
            print("{} has no metric".format(name))
            metric = "50.0"
        try:
            metric = str(int(float(metric)/5))
        except ValueError:
            print("metric {} was corrected to \"10\"".format(metric))
            metric = "10"
        if hospital["model_x"] and hospital["model_y"]:
            if (0 <= hospital["model_x"] <= 259) and (0 <= hospital["model_y"] <= 259) :
                rows.append([str(int(hospital["model_x"])),
                             str(int(hospital["model_y"])),
                             str(int(metric))])
    with open(sys.argv[3], "w") as fd:
        fd.write("\n".join([" ".join(row) for row in rows]))

if __name__ == "__main__":
    main()
