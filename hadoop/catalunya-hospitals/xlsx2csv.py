#!/usr/bin/env python3
# -*-coding: utf-8-*-

import sys

import openpyxl

def main():
    if len(sys.argv) != 3:
        print("usage: {} xlsx_file csv_file".format(sys.argv[0]))
        sys.exit(1)

    wb = openpyxl.load_workbook(sys.argv[1])
    ws = wb.active
    with open(sys.argv[2], "w") as fd:
        for row in ws.iter_rows(min_row=2, max_col=14):
            fd.write("\t".join([str(cell.value).replace("\t", " ") for cell in row])+"\n")

if __name__ == "__main__":
    main()
