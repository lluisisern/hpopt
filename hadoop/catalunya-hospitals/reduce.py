#!/usr/bin/env python3
# -*-coding: utf-8-*-

import json
import sys

def merge(ldict, rdict):
    for key in rdict:
        if key in ldict:
            ldict[key].update(rdict[key])
        else:
            ldict.update(rdict)

def main():
    if len(sys.argv) == 1:
        input = sys.stdin
        output_hospitals = sys.stdout
        output_metrics = sys.stdout
    elif len(sys.argv) == 4:
        input = open(sys.argv[1])
        output_hospitals = open(sys.argv[2], "w")
        output_metrics = open(sys.argv[3], "w")
    else:
        print("usage: {} [input output_hospitals output_metrics]".format(sys.argv[0]))
        sys.exit(1)

    metrics = {}
    hospitals = {}
    for row in input:
        row = row.strip().split(sep="\t")
        obj = json.loads(row[1])
        if row[0] == "metrics":
            merge(metrics, obj)
        elif row[0] == "hospitals":
            merge(hospitals, obj)

    print("hospitals", json.dumps(hospitals, ensure_ascii=False), sep="\t", file=output_hospitals)
    print("metrics", json.dumps(metrics, ensure_ascii=False), sep="\t", file=output_metrics)

if __name__ == "__main__":
    main()
