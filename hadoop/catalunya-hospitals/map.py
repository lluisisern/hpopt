#!/usr/bin/env python3
# -*-coding: utf-8-*-

import json
import sys

def main():
    for row in sys.stdin:
        row = row.strip().split(sep="\t")
        print("metrics", json.dumps({row[2]: {row[6]: row[7]}}, ensure_ascii=False), sep="\t")
        print("hospitals", json.dumps({row[2]: {"Hospital CDR": row[4],
                                                "Entitat Proveïdora": row[5],
                                                "ID U. Proveïdora": row[1],
                                                "Regió": row[3]}}, ensure_ascii=False), sep="\t")

if __name__ == "__main__":
    main()
