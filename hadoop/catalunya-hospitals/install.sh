#!/bin/bash
aws s3 cp s3://hpopt-deploy/hadoop/catalunya-hospitals/xlsx2csv.py .
aws s3 cp s3://hpopt-deploy/hadoop/catalunya-hospitals/geolocate.py .
aws s3 cp s3://hpopt-deploy/hadoop/catalunya-hospitals/requirements.txt .
chmod +x xlsx2csv.py
chmod +x geolocate.py
sudo pip-3.4 install -r requirements.txt
