#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import pwd
import json
import os
import grp
import shlex

import boto3
import daemon
import daemon.pidlockfile
import requests

import pipeline


def make_bootstrap_action(action):
    if "args" in action:
        if isinstance(action["args"], str):
            action["args"] = shlex.split(action["args"])
        elif not isinstance(action["args"], list):
            raise ValueError("expected 'args': 'string'|'list'")
    else:
        action["args"] = []
    return {"Name": action["name"],
            "ScriptBootstrapAction": {
                "Path": action["exec"],
                "Args": action["args"]}}


def make_streaming_step(step):
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["hadoop-streaming",
                         "-files", ",".join((step["exec"]["mapper"], step["exec"]["reducer"])),
                         "-mapper", os.path.basename(step["exec"]["mapper"]),
                         "-reducer", os.path.basename(step["exec"]["reducer"]),
                         "-input", step["input"],
                         "-output", step["output"]]}}


def make_hive_step(step):
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["hive-script", "--run-hive-script", "--hive-versions", "2.1.0", "--args"
                         "-f", step["exec"],
                         "-d", "INPUT={}".format(step["input"]),
                         "-d", "OUTPUT={}".format(step["output"])]}}


def make_pig_step(step):
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["pig-script", "--run-pig-script", "--args",
                         "-f", step["exec"],
                         "-p", "INPUT={}".format(step["input"]),
                         "-p", "OUTPUT={}".format(step["output"])]}}

def make_spark_step(step):
    if "spark_args" in step:
        if isinstance(step["spark_args"], str):
            step["spark_args"] = shlex.split(step["spark_args"])
        elif not isinstance(step["spark_args"], list):
            raise ValueError("expected 'spark_args': 'string'|'list'")
    else:
        step["spark_args"] = []
    if "args" in step:
        if isinstance(step["args"], str):
            step["args"] = shlex.split(step["args"])
        elif not isinstance(step["args"], list):
            raise ValueError("expected 'args': 'string'|'list'")
    else:
        step["args"] = []
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["spark-submit",] + step["spark_args"] +
                        [step["exec"],] + step["args"]}}

def make_command_step(step):
    if "args" in step:
        if isinstance(step["args"], str):
            step["args"] = shlex.split(step["args"])
        elif not isinstance(step["args"], list):
            raise ValueError("expected 'args': 'string'|'list'")
    else:
        step["args"] = []
    return {"Name": step["name"],
                "ActionOnFailure": "CONTINUE",
                "HadoopJarStep": {
                    "Jar": "command-runner.jar",
                    "Args": [step["exec"],] + step["args"]}}

def make_custom_step(step):
    if "args" in step:
        if isinstance(step["args"], str):
            step["args"] = shlex.split(step["args"])
        elif not isinstance(step["args"], list):
            raise ValueError("expected 'args': 'string'|'list'")
    else:
        step["args"] = []
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": step["jar"],
                "MainClass": step["main_class"],
                "Args": step["args"]}}

def make_s3distcp_step(step):
    return {"Name": step["name"],
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["s3-dist-cp",
                         "--src", step["input"],
                         "--dest", step["output"]] +
                step["args"]
            }}

def make_status_step(message):
    return {"Name": "hpopt notify status",
            "ActionOnFailure": "CONTINUE",
            "HadoopJarStep": {
                "Jar": "command-runner.jar",
                "Args": ["s3://" + os.path.join(pipeline.names["deploy"]["s3"], "scrape2status.py"),
                         json.dumps(message)]}}

def make_simulate_step(message):
    return {
        "Name": "hpopt notify simulate",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": ["s3://" + os.path.join(pipeline.names["deploy"]["s3"], "scrape2simulate.py"),
                     json.dumps(message)]}}

class ScrapingConfiguration(dict):
    ACCEPTED_TYPES = "streaming", "hive", "pig", "spark", "custom", "command", "s3distcp"
    def __init__(self, message, metadata):
        super().__init__()

        msg = message["scrape"]

        # name is mandatory!
        if isinstance(msg["name"], str):
            self["Name"] = msg["name"]
        else:
            raise ValueError("expected 'name': 'string'")

        # keep logs or not
        if "logs" in msg:
            if isinstance(msg["logs"], str):
                if msg["logs"]:
                    self["LogUri"] = msg["logs"]
            else:
                raise ValueError("expected 'logs': 'string'")

        self["JobFlowRole"] = pipeline.names["iam_roles"]["scrape_emr"]
        self["ServiceRole"] = pipeline.names["iam_roles"]["scrape_emr_ec2"]

        # TODO: EC2 SGs?
        #self["EmrManagedMasterSecurityGroup"] = ""
        #self["EmrManagedSlaveSecurityGroup"] = ""
        #self["ServiceAccessSecurityGroup"] = ""

        # set ec2 inst types
        self["Instances"] = {
            "MasterInstanceType": metadata["master_inst_type"],
            "SlaveInstanceType": metadata["slave_inst_type"],
            "KeepJobFlowAliveWhenNoSteps": False}

        if isinstance(msg["cores"], int):
            self["Instances"]["InstanceCount"] = msg["cores"] + 1
        else:
            raise ValueError("expected 'cores': integer")

        if "tasks" in msg:
            if isinstance(msg["tasks"], int):
                if msg["tasks"] > 0:
                    self["Instances"]["InstanceGroups"] = [{
                        "Name": "tasks group",
                        "Market": "SPOT",
                        "InstanceRole": "TASK",
                        "InstanceCount": msg["tasks"],
                        "BidPrice": metadata["bid_price"],
                        "InstanceType": self["Instances"]["SlaveInstanceType"],
                        "EbsConfiguration": {
                            "EbsBlockDeviceConfigs": [{"VolumeSpecification": {"VolumeType": "gp2",
                                                                               "SizeInGB": metadata["slave_ebs_size"]}}]}}]
            else:
                raise ValueError("expected 'tasks': integer")

            # add bootstrap actions
            if "bootstrap" in msg:
                if isinstance(msg["bootstrap"], list):
                    self["BootstrapActions"] = []
                    for action in msg["bootstrap"]:
                        if isinstance(action, dict):
                            self["BootstrapActions"].append(make_bootstrap_action(action))
                        else:
                            raise ValueError("expected 'bootstrap': [ { ... }, ... ]")
                else:
                    raise ValueError("expected 'bootstrap': [ ... ]")

        # add steps
        if isinstance(msg["steps"], list):
            self["Steps"] = []
            for step in msg["steps"]:
                if isinstance(step, dict):
                    if step["type"] not in ScrapingConfiguration.ACCEPTED_TYPES:
                        raise ValueError("|".join(["'{}'".format(t) for t in ScrapingConfiguration.ACCEPTED_TYPES]))
                    elif step["type"] == "streaming": self["Steps"].append(make_streaming_step(step))
                    elif step["type"] == "hive": self["Steps"].append(make_hive_step(step))
                    elif step["type"] == "pig": self["Steps"].append(make_pig_step(step))
                    elif step["type"] == "spark": self["Steps"].append(make_spark_step(step))
                    elif step["type"] == "command": self["Steps"].append(make_command_step(step))
                    elif step["type"] == "custom": self["Steps"].append(make_custom_step(step))
                else:
                    raise ValueError("expected 'steps': [ { ... }, ... ]")
        else:
            raise ValueError("expected 'steps': [ ... ]")

        # report to status-queue and simulation-queue
        self["Steps"].append(make_status_step(message))
        if "simulate" in message:
            self["Steps"].append(make_simulate_step(message))


def retrieve_metadata():
    tags = boto3.resource("ec2").Instance(
        requests.get("http://169.254.169.254/latest/meta-data/instance-id").content).tags
    return {"bid_price": int(tags["hpopt_BidPrice"]),
            "master_inst_type": tags["hpopt_MasterInstType"],
            "slave_inst_type": tags["hpopt_SlaveInstType"],
            "slave_ebs_size": int(tags["hpopt_SlaveEbsSize"])}


def main():
    print("{} started at {}".format(__file__, datetime.datetime.now()))

    metadata = retrieve_metadata()

    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["scrape"]["sqs"])
    while True:
        for message in sqs.receive_messages():
            print("message received!\n{}\n".format(message.body))
            message.delete()
            try:
                message = json.loads(message.body)
            except ValueError:
                print("WARNING: message body does not contain a valid JSON")
                continue
            if not isinstance(message, dict):
                print("WARNING: message body doesn't contain a JSON object")
                continue

            # parse config
            try:
                config = ScrapingConfiguration(message, metadata)
            except ValueError as e:
                print("WARNING: invalid configuration!", str(e))
                continue

            # launch cluster
            boto3.client("emr").run_job_flow(**config)


if __name__ == "__main__":
    with open("/var/log/hpopt/scrape.log", "w") as fd:
        with daemon.DaemonContext(uid=pwd.getpwnam("hpopt").pw_uid,
                                  gid=grp.getgrnam("hpopt").gr_gid,
                                  pidfile=daemon.pidlockfile.PIDLockFile(os.path.join(os.environ["HOME"], ".scrape.pid")),
                                  stdout=fd, stderr=fd):
            main()
