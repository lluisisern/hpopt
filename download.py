#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import grp
import json
import os
import pwd
import tempfile
import threading

import boto3
import botocore
import daemon
import daemon.pidlockfile
import requests

import pipeline
from sync import sync_dir

#TODO: add an inactivity timeout


class DownloadWorker(threading.Thread):
    def __init__(self, message):
        super().__init__(daemon=True)
        self.message = message
        self.files = message["source"]["files"]
        self.name = message["source"]["name"]


    def run(self):
        try:
            for file in self.files:
                r = requests.get(file["url"], stream=True)
                os.makedirs(os.path.join(os.environ["HOME"], "sources", self.name), exist_ok=True)
                with open(os.path.join(os.environ["HOME"], "sources", self.name, file["filename"]), "wb") as fd:
                    for chunk in r.iter_content(chunk_size=1024**2):
                        fd.write(chunk)
            sync_dir(pipeline.names["source"]["s3"], delete=True,
                     local_src=os.path.join(os.environ["HOME"], "sources", self.name),
                     remote_dst=os.path.join("sources", self.name))
            if "scrape" in self.message:
                # propagate message to the next stage (scrape, in this case)
                sqs_scrape = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["scrape"]["sqs"])
                sqs_scrape.send_message(MessageBody=json.dumps(self.message))
                # and notify master
                sqs_status = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["status"]["sqs"])
                sqs_status.send_message(MessageBody=json.dumps({"name": self.name,
                                                                "type": "source",
                                                                "status": "success"}))
        except (IOError,botocore.exceptions.ClientError) as e:
            print(e)
            sqs_status = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["status"]["sqs"])
            sqs_status.send_message(MessageBody=json.dumps({"name": self.name,
                                                            "type": "source",
                                                            "status": "failure"}))


def is_network_maxed():
    #TODO: use ethtool|grep 'Speed:' and compare to actual speed
    # for now, assume threads manage to use full network speed
    return True


def download_stream(message):
    msg = message["source"]
    s3 = boto3.client("s3")

    for file in msg["files"]:
        s3mp = s3.create_multipart_upload(Bucket=pipeline.names["source"]["s3"],
                                          Key=os.path.join("sources", message["name"], file["filename"]))
        r = requests.get(file["url"], stream=True)
        os.makedirs(os.path.join(os.environ["HOME"], "sources", msg["name"]), exist_ok=True)

        size_max = 1024**3  # 1 GB
        size = 0
        cur_part = 0
        content = r.iter_content(chunk_size=1024**2) # 1 MB
        with tempfile.TemporaryFile() as tmp:
            try:
                while True:
                    chunk = next(content)
                    size += len(chunk)
                    tmp.write(chunk)
                    if size >= size_max:
                        tmp.seek(0)
                        s3.upload_part(Body=tmp,
                                       Bucket=pipeline.names["source"],
                                       PartNumber=cur_part,
                                       Key=os.path.join("sources", message["name"], file["filename"]),
                                       UploadId=s3mp["UploadId"])
                        cur_part += 1
                        size = 0
            except StopIteration:
                break # download finished


def main():
    print("{} started at {}".format(__file__, datetime.datetime.now()))
    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["source"]["sqs"])

    active_threads = set()
    while True:
        # remove dead threads from active_threads set
        dead = set()
        for thread in active_threads:
            if not thread.is_alive():
                dead.add(thread)
        active_threads = active_threads.difference(dead)

        for message in sqs.receive_messages():
            print("message received!\n{}\n".format(message.body))
            # we only grab the message if we are not running other threads,
            # or if the active threads are not maxing out uplink capacity
            if len(active_threads) == 0 or not is_network_maxed():
                message.delete()
            else:
                continue
            try:
                message = json.loads(message.body)
            except ValueError:
                print("WARNING: message body does not contain a valid JSON")
                continue
            if not isinstance(message, dict):
                print("WARNING: message body doesn't contain a JSON object")
                continue

            thread = DownloadWorker(message)
            thread.start()
            active_threads.add(thread)


if __name__ == "__main__":
    with open("/var/log/hpopt/download.log", "w") as fd:
        with daemon.DaemonContext(uid=pwd.getpwnam("hpopt").pw_uid,
                                  gid=grp.getgrnam("hpopt").gr_gid,
                                  pidfile=daemon.pidlockfile.PIDLockFile(os.path.join(os.environ["HOME"], ".download.pid")),
                                  stdout=fd, stderr=fd):
            main()
    autoscaling = boto3.client("autoscaling")
    autoscaling.execute_policy(AutoScalingGroupName=pipeline.names["asg"],
                               PolicyName=pipeline.names["asg_scalein"],
                               HonorCooldown=True)
    os.system("sudo poweroff")
