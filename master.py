#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import grp
import json
import os
import pwd

import boto3
import daemon
import daemon.pidlockfile

import pipeline


def merge_dicts(*dicts):
    result = {}
    for d in dicts:
        if not d.keys().isdisjoint(result):
            print("WARNING: config keys are being overriden, something is not right")
        result.update(d)
    return result


def main():
    print("{} started at {}".format(__file__, datetime.datetime.now()))
    main_sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["main"]["sqs"])
    while True:
        for message in main_sqs.receive_messages():
            print("message received!\n{}\n".format(message.body))
            message.delete()
            try:
                message = json.loads(message.body)
            except ValueError:
                print("WARNING: message body does not contain a valid JSON")
                continue

            if not isinstance(message, dict):
                print("WARNING: message body doesn't contain a JSON object")
                continue

            try:
                source = message["source"]
            except KeyError:
                source = None
            try:
                scrape = message["scrape"]
            except KeyError:
                scrape = None
            try:
                simulate = message["simulate"]
            except KeyError:
                simulate = None

            if not source and not scrape and not simulate:
                print("at least one source, scrape, or simulate job must be specified")
                continue
            if source and simulate and not scrape:
                print("source and simulate job specified, but not scrape. you cannot feed source data directly into the simulate stage")
                # TODO: add noop 'scrape' stage
                continue

            # load and merge required configuration files,
            # and find earliest stage of the pipeline to which send the SQS message
            conf_source, conf_scrape, conf_simulate = {}, {}, {}
            sqs = None
            if source:
                try:
                    with open(os.path.join(os.environ["HOME"], "config", "source", source)) as fd:
                        conf_source = json.load(fd)
                except FileNotFoundError:
                    print("WARNING: source {} doesn't exist!".format(source))
                    continue
                if not sqs:
                    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["source"]["sqs"])
            if scrape:
                try:
                    with open(os.path.join(os.environ["HOME"], "config", "scrape", scrape)) as fd:
                        conf_scrape = json.load(fd)
                except FileNotFoundError:
                    print("WARNING: scrape {} doesn't exist!".format(scrape))
                    continue
                if not sqs:
                    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["scrape"]["sqs"])
            if simulate:
                try:
                    with open(os.path.join(os.environ["HOME"], "config", "simulate", simulate)) as fd:
                        conf_simulate = json.load(fd)
                except FileNotFoundError:
                    print("WARNING: simulation {} doesn't exist!".format(simulate))
                    continue
                if not sqs:
                    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["simulate"]["sqs"])

            assert sqs, "sqs entry point was not set!"
            print("sending message to sqs: {}".format(sqs.url))
            sqs.send_message(MessageBody=json.dumps(merge_dicts(conf_source, conf_scrape, conf_simulate)))


if __name__ == "__main__":
    with open("/var/log/hpopt/master.log", "w") as fd:
        with daemon.DaemonContext(uid=pwd.getpwnam("hpopt").pw_uid,
                                  gid=grp.getgrnam("hpopt").gr_gid,
                                  pidfile=daemon.pidlockfile.PIDLockFile(os.path.join(os.environ["HOME"], ".master.pid")),
                                  stdout=fd, stderr=fd):
            main()
