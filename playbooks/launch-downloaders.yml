---
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

# This playbook launches an AutoScaling group,
# and starts the hpopt-download daemon on it

- name: launch Download EC2 AutoScaling Group
  hosts: localhost
  connection: local
  gather_facts: false

  tasks:

    - name: "read pipeline-names.json"
      include_vars:
        file: ../pipeline-names.json
        name: pipeline

    - name: "create {{ pipeline.source.asg_lc }} Launch Configuration"
      ec2_lc:
        name: "{{ pipeline.source.asg_lc }}"
        state: present
        region: "{{ pipeline.region }}"
        image_id: ami-f9dd458a
        key_name: isern-tfg
        instance_profile_name: hpopt-source
        security_groups: ["testSG"]
        instance_type: t2.micro
        user_data: "{{ lookup('file', '../bootstrap/download.sh') }}"
        volumes:
        - device_name: /dev/xvda
          volume_size: 128
          device_type: gp2
          delete_on_termination: true

    - name: "launch {{ pipeline.source.asg }} AutoScaling Group"
      ec2_asg:
        name: "{{ pipeline.source.asg }}"
        launch_config_name: "{{ pipeline.source.asg_lc }}"
        region: "{{ pipeline.region }}"
        max_size: 10
        min_size: 0
        desired_capacity: 0
        default_cooldown: 60

    - name: "create {{ pipeline.source.asg_scaleout }} ASG policy"
      register: source_scaleout_policy
      ec2_scaling_policy:
        asg_name: "{{ pipeline.source.asg }}"
        name: "{{ pipeline.source.asg_scaleout }}"
        region: "{{ pipeline.region }}"
        cooldown: 60
        adjustment_type: ChangeInCapacity
        scaling_adjustment: 1

    - name: "create {{ pipeline.source.asg_scalein }} ASG policy"
      ec2_scaling_policy:
        asg_name: "{{ pipeline.source.asg }}"
        name: "{{ pipeline.source.asg_scalein }}"
        region: "{{ pipeline.region }}"
        cooldown: 60
        adjustment_type: ChangeInCapacity
        scaling_adjustment: -1

    - name: "create {{ pipeline.source.asg_scaleout }} CloudWatch alarm"
      ec2_metric_alarm:
        region: "{{ pipeline.region }}"
        state: present
        name: "{{ pipeline.source.asg_scaleout }}"
        metric: "ApproximateNumberOfMessagesVisible"
        statistic: "Average"
        comparison: ">="
        threshold: 1.0
        period: 60
        evaluation_periods: 2
        dimensions:
          QueueName: "{{ pipeline.source.sqs }}"
        alarm_actions:
          - "{{ source_scaleout_policy.arn }}"
