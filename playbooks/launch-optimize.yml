---
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

# This playbook launches an EC2 instance,
# deploys hpopt, and starts the optimize daemon

- name: launch Optimize EC2 node
  hosts: localhost
  connection: local
  gather_facts: false

  tasks:

    - name: "read pipeline-names.json"
      include_vars:
        file: ../pipeline-names.json
        name: pipeline

    - name: launch Optimize EC2 instance
      register: ec2_optimize
      ec2:
        instance_tags:
          Name: Optimize
          hpopt_EndpointAddr: 1.2.3.4
          hpopt_EndpointUser: myuser
        key_name: isern-tfg
        instance_type: t2.micro
        image: ami-f9dd458a
        region: "{{ pipeline.region }}"
        exact_count: 1
        count_tag:
          Name: Optimize
        group: ['testSG']
        instance_profile_name: hpopt-optimize
        wait: yes

    - name: wait for SSH
      wait_for:
        host: "{{ ec2_optimize.tagged_instances[0].public_ip }}"
        port: 22
        timeout: 120

    - name: refresh EC2 dynamic inventory
      command: /etc/ansible/hosts --refresh-cache
    - meta: refresh_inventory

- name: install hpopt to Optimize EC2 node
  hosts: tag_Name_Optimize
  remote_user: ec2-user
  become: yes
  become_user: root
  gather_facts: no

  tasks:

    - name: "read pipeline-names.json"
      include_vars:
        file: ../pipeline-names.json
        name: pipeline

    - name: "save hpopt-latest.rpm from {{ pipeline.deploy.s3 }} S3 bucket"
      s3:
        bucket: "{{ pipeline.deploy.s3 }}"
        mode: get
        overwrite: different
        object: rpm/hpopt-latest.rpm
        dest: "/root/hpopt-latest.rpm"

    - name: yum localinstall hpopt-latest.rpm
      yum:
        name: "/root/hpopt-latest.rpm"
        state: present

    - name: install pip-3.4 dependencies
      pip:
        executable: /usr/bin/pip-3.4
        requirements: /usr/share/hpopt/requirements.txt

    - name: configure AWS region
      command: "aws configure set region {{ pipeline.region }}"
      become_user: hpopt

    - name: start hpopt-optimize daemon
      service:
        name: hpopt-optimize
        enabled: yes
        state: started
