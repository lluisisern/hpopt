Name:      hpopt
Version:   %VERSION%
Release:   %RELEASE%
Summary:   Daemons to run the various nodes of hpopt
License:   GPLv3
BuildArch: noarch
Source:    ~/rpmbuild/SOURCES/hpopt-%{version}-%{release}.tar.gz
URL:       https://bitbucket.org/lluisisern/hpopt
Requires:  python34, python34-pip, aws-cli, libffi-devel, gcc, libcurl-devel, python34-devel, openssl-devel
BuildRoot: %{_tmppath}%{name}-%{version}-%{release}

%description
Daemons to run the Master, Download, Scrape, and Optimize  nodes of the hpopt infrastructure.
Includes a script to synchronize configuration files.

%prep
%setup -q

%pre
getent  group hpopt >/dev/null || groupadd -r hpopt
getent passwd hpopt >/dev/null || useradd -r -g hpopt -s /sbin/nologin \
    -d /etc/hpopt hpopt

%install
rm -rf %{buildroot}

install -d %{buildroot}%{_datadir}/hpopt
install -d %{buildroot}%{_sysconfdir}/hpopt
install -d %{buildroot}/var/log/hpopt

[ -d %{buildroot}%{_initddir} ] || install -d %{buildroot}%{_initddir}
[ -d %{buildroot}%{_bindir} ]   || install -d %{buildroot}%{_bindir}

install initscripts/hpopt-download %{buildroot}%{_initddir}
install initscripts/hpopt-master   %{buildroot}%{_initddir}
install initscripts/hpopt-optimize %{buildroot}%{_initddir}
install initscripts/hpopt-scrape   %{buildroot}%{_initddir}
install initscripts/hpopt-status   %{buildroot}%{_initddir}
install download.py         %{buildroot}%{_datadir}/hpopt
install master.py           %{buildroot}%{_datadir}/hpopt
install optimize.py         %{buildroot}%{_datadir}/hpopt
install pipeline.py         %{buildroot}%{_datadir}/hpopt
install scrape.py           %{buildroot}%{_datadir}/hpopt
install status.py           %{buildroot}%{_datadir}/hpopt
install sync.py             %{buildroot}%{_datadir}/hpopt
install pipeline-names.json %{buildroot}%{_datadir}/hpopt
install requirements.txt    %{buildroot}%{_datadir}/hpopt
touch %{buildroot}/var/log/hpopt/download.log
touch %{buildroot}/var/log/hpopt/master.log
touch %{buildroot}/var/log/hpopt/optimize.log
touch %{buildroot}/var/log/hpopt/scrape.log
touch %{buildroot}/var/log/hpopt/status.log

ln -s %{_datadir}/hpopt/download.py %{buildroot}%{_bindir}/hpopt-download
ln -s %{_datadir}/hpopt/master.py   %{buildroot}%{_bindir}/hpopt-master
ln -s %{_datadir}/hpopt/optimize.py %{buildroot}%{_bindir}/hpopt-optimize
ln -s %{_datadir}/hpopt/scrape.py   %{buildroot}%{_bindir}/hpopt-scrape
ln -s %{_datadir}/hpopt/status.py   %{buildroot}%{_bindir}/hpopt-status
ln -s %{_datadir}/hpopt/sync.py     %{buildroot}%{_bindir}/hpopt-sync

%post
echo "hpopt ALL=(ALL) NOPASSWD: /sbin/poweroff">/etc/sudoers.d/hpopt

%clean
rm -rf %{buildroot}

%files
%attr(0755, hpopt, hpopt) %{_sysconfdir}/hpopt/
%attr(0755, root, root) %{_initddir}/hpopt-download
%attr(0755, root, root) %{_initddir}/hpopt-master
%attr(0755, root, root) %{_initddir}/hpopt-optimize
%attr(0755, root, root) %{_initddir}/hpopt-scrape
%attr(0755, root, root) %{_initddir}/hpopt-status
%attr(-, root, root) %{_bindir}/hpopt-download
%attr(-, root, root) %{_bindir}/hpopt-master
%attr(-, root, root) %{_bindir}/hpopt-optimize
%attr(-, root, root) %{_bindir}/hpopt-scrape
%attr(-, root, root) %{_bindir}/hpopt-status
%attr(-, root, root) %{_bindir}/hpopt-sync
%attr(0755, root, root) %{_datadir}/hpopt/download.py
%attr(0755, root, root) %{_datadir}/hpopt/master.py
%attr(0755, root, root) %{_datadir}/hpopt/optimize.py
%attr(0755, root, root) %{_datadir}/hpopt/pipeline.py
%attr(0755, root, root) %{_datadir}/hpopt/status.py
%attr(0755, root, root) %{_datadir}/hpopt/scrape.py
%attr(0755, root, root) %{_datadir}/hpopt/sync.py
%attr(0644, root, root) %{_datadir}/hpopt/pipeline-names.json
%attr(0644, root, root) %{_datadir}/hpopt/requirements.txt
%attr(0644, hpopt, root) /var/log/hpopt/download.log
%attr(0644, hpopt, root) /var/log/hpopt/master.log
%attr(0644, hpopt, root) /var/log/hpopt/optimize.log
%attr(0644, hpopt, root) /var/log/hpopt/scrape.log
%attr(0644, hpopt, root) /var/log/hpopt/status.log

%changelog
