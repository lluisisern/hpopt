#!/usr/bin/env python3
# -*-coding: utf-8-*-
#
# Copyright (C) 2016  Lluís Isern Bennassar
#
# This file is part of hpopt.
#
# hpopt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# hpopt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with hpopt.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import grp
import json
import os
import pwd
import shlex
import tempfile

import boto3
import daemon
import daemon.pidlockfile
import paramiko
import requests
from scp import SCPClient

import pipeline
from sync import sync_dir


def run_command(ssh, command):
    cmd = ssh.exec_command(command)
    cmd[0].close()
    return {"stdout": cmd[1].read().decode("utf-8"),
            "stderr": cmd[2].read().decode("utf-8")}


def copy_model(ssh, msg):
    model = msg["model"]
    if not isinstance(model, dict):
        raise ValueError("expected 'model': { ... }")

    with tempfile.TemporaryDirectory() as tmpdir:
        sync_dir(pipeline.names["deploy"]["s3"],
                 remote_src=model["path"],
                 local_dst=tmpdir)

        run_command(ssh, "rm -rf {}".format(shlex.quote(msg["name"])))
        run_command(ssh, "mkdir -p {}".format(shlex.quote(msg["name"])))

        for tmp in os.listdir(tmpdir):
            with SCPClient(ssh.get_transport()) as scp:
                scp.put(os.path.join(tmpdir, tmp), msg["name"], recursive=True)


def run_configs(ssh, msg):
    configs = msg["config"]
    if isinstance(configs, str):
        configs = [configs]
    elif not isinstance(configs, list):
        raise ValueError("expected 'config': '...'|[ ... ]")

    if "args" in msg["model"]:
        if isinstance(msg["model"]["args"], list):
            args = " ".join([shlex.quote(arg) for arg in msg["model"]["args"]])
        elif isinstance(msg["model"]["args"], str):
            args = msg["model"]["args"]
        else:
            raise ValueError("expected 'model': { 'args': '...'|[ ... ] }")
    else:
        args = ""

    if "wrap" in msg["model"]:
        if isinstance(msg["model"]["wrap"], str):
            wrap = msg["model"]["wrap"]
        else:
            raise ValueError("expected 'model': { 'wrap': '...' }")
        run_command(ssh, "chmod +x {}".format(shlex.quote(os.path.join(msg["name"], wrap)))) # USELESS!
    else:
        wrap = ""

    for config in configs:
        with tempfile.TemporaryDirectory() as tmpdir:
            boto3.resource("s3").Bucket(pipeline.names["deploy"]["s3"])\
                .download_file(config, os.path.join(tmpdir, os.path.basename(config)))
            with SCPClient(ssh.get_transport()) as scp:
                scp.put(os.path.join(tmpdir, os.path.basename(config)), msg["name"], recursive=True)

        cmd = run_command(ssh, "cd {}; {}; {}".format(
            shlex.quote(msg["name"]),
            "source {}".format(shlex.quote(wrap)),
            " ".join((shlex.quote("./{}".format(msg["model"]["exec"])),
                      shlex.quote(os.path.basename(config)))),
            args))

        print(cmd["stdout"])
        print(cmd["stderr"])


def run_prep(ssh, msg):
    if "prep" not in msg["model"]:
        return
    preps = msg["model"]["prep"]
    if isinstance(preps, str):
        preps = [preps]
    elif not isinstance(preps, list):
        raise ValueError("expected 'prep': '...'|[ ... ]")

    for prep in preps:
        cmd = run_command(ssh, prep)
        print(cmd["stdout"])
        print(cmd["stderr"])


def run_post(ssh, msg):
    if "post" not in msg["model"]:
        return
    posts = msg["model"]["post"]
    if isinstance(posts, str):
        posts = [posts]
    elif not isinstance(posts, list):
        raise ValueError("expected 'post': '...'|[ ... ]")

    for post in posts:
        cmd = run_command(ssh, post)
        print(cmd["stdout"])
        print(cmd["stderr"])


def copy_inputs(ssh, msg):
    if "input" not in msg: return
    if isinstance(msg["input"], list):
        inputs = msg["input"]
    else:
        raise ValueError("expected 'input': [ { ... }, ... ]")

    for input in inputs:
        if not isinstance(input, dict):
            raise ValueError("expected 'input': [ { 'src': '...', 'dst': '...' }, ... ]")
        with tempfile.TemporaryDirectory() as tmpdir:
            if input["src"].endswith("/"):
                sync_dir(pipeline.names["scrape"]["s3"],
                         remote_src=input["src"],
                         local_dst=tmpdir)
            else:
                boto3.resource("s3").Bucket(pipeline.names["scrape"]["s3"])\
                    .download_file(input["src"], os.path.join(tmpdir, os.path.basename(input["src"])))
            for tmp in os.listdir(tmpdir):
                with SCPClient(ssh.get_transport()) as scp:
                    scp.put(os.path.join(tmpdir, tmp), os.path.join(msg["name"], input["dst"]), recursive=True)


def copy_outputs(ssh, msg):
    if "output" not in msg: return
    if isinstance(msg["output"], list):
        outputs = msg["output"]
    else:
        raise ValueError("expected 'output': [ { ... }, ... ]")

    for output in outputs:
        if not isinstance(output, dict):
            raise ValueError("expected 'output': [ { 'src': '...', 'dst': '...' }, ... ]")
        with tempfile.TemporaryDirectory() as tmpdir:
            with SCPClient(ssh.get_transport()) as scp:
                scp.get(os.path.join(msg["name"], output["src"]), tmpdir, recursive=True)
            for tmp in os.listdir(tmpdir):
                if os.path.isdir(tmp):
                    sync_dir(pipeline.names["simulate"]["s3"],
                             local_src=os.path.join(tmpdir, tmp),
                             remote_dst=os.path.join(msg["name"], output["dst"]))
                else:
                    boto3.resource("s3").Bucket(pipeline.names["simulate"]["s3"])\
                        .upload_file(os.path.join(tmpdir, tmp), os.path.join(msg["name"], output["dst"]))


def retrieve_metadata():
    tags = boto3.resource("ec2").Instance(
        requests.get("http://169.254.169.254/latest/meta-data/instance-id").content).tags
    return {"endpoint_addr": tags["hpopt_EndpointAddr"],
            "endpoint_user": tags["hpopt_EndpointUser"]}


def main():
    print("{} started at {}".format(__file__, datetime.datetime.now()))

    metadata = retrieve_metadata()

    sqs = boto3.resource("sqs").get_queue_by_name(QueueName=pipeline.names["simulate"]["sqs"])
    while True:
        for message in sqs.receive_messages():
            print("message received!\n{}\n".format(message.body))
            message.delete()
            try:
                message = json.loads(message.body)
            except ValueError:
                print("WARNING: message body does not contain a valid JSON")
                continue
            if not isinstance(message, dict):
                print("WARNING: message body does not contain a JSON object")
                continue

            msg = message["simulate"]
            if "name" not in msg or "model" not in msg or "config" not in msg:
                print("WARNING: at least 'name', 'model', and 'config' must be specified")
                continue

            ssh = None
            try:
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(metadata["endpoint_addr"], username=metadata["endpoint_user"], key_filename="???")
                copy_model(ssh, msg)
                copy_inputs(ssh, msg)
                run_prep(ssh, msg)
                run_configs(ssh, msg)
                run_post(ssh, msg)
                copy_outputs(ssh, msg)
            except ValueError:
                print("WARNING: message body does not contain a valid configuration")
                continue
            finally:
                if ssh:
                    ssh.close()


if __name__ == "__main__":
    with open("/var/log/hpopt/optimize.log", "w") as fd:
        with daemon.DaemonContext(uid=pwd.getpwnam("hpopt").pw_uid,
                                  gid=grp.getgrnam("hpopt").gr_gid,
                                  pidfile=daemon.pidlockfile.PIDLockFile(os.path.join(os.environ["HOME"], ".optimize.pid")),
                                  stdout=fd, stderr=fd):
            main()
